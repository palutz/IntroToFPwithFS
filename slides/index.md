- title : Functional Programming with F#
- description : Exploring the functional programming paradigm using F#
- author : Stefano Paluello
- theme : moon
- transition : default

***

## Down the rabbit hole...

<br />
<br />

#### Intro to functional programming with F#

<br />
<br />
Stefano Paluello - [@palutz](http://www.twitter.com/palutz)

***

# Part 1

---

### Fast Recap

* What is Functional Programming?
    * Immutabiliy 
    * Evaluation of expressions
    * Functions as first-class citizen (HOF)
    * Purity (pure functions)
    * Referential transparency


---

### Fast Recap (2)

* What is Functional Programming? (2)
    * Currying
    * Partial Application
    * Function composition

---

## Why F# ?


 <img src="images/languages.png" style="background: white;" width=700 />

---

### F# is
  
* Hybrid, functional first
* Strongly typed
* Mature, based on the ML family
* Open source (comes from Microsoft Research)
* Cross-platform (Mono and .Net Core)

***

# F# 101

#### Tooling in an open source World.

---

### F# First Project in Dotnet core

    λ.> dotnet new console -lang F# -o helloWorld
    λ.> cd helloWorld
    [helloWorld] λ.> dotnet run

    Hello World from F#!

---

### F# REPL

*Read Evaluate Print Loop*


(or F# Interactive)

     
    [~] λ.> fsharpi

    Microsoft (R) F# Interactive version 4.1
    Copyright (c) Microsoft Corporation. All Rights Reserved.
    For help type #help;;

    > let a = 3;;
    val a : int = 3

    > let s = "Stefano";;
    val s : string = "Stefano"
     

*[Linux/Mac OsX version]*

***

# Hello F#


##### A glympse of the F# syntax

---

### Simple let bindings

    let a = 1   // Binding 1 (int) to label a

    let x = 50.6  // Binding 50.6 (float) to label x

    a = a + 1 // ERROR!!! Immutable value

    let b = a + 1 // Binding the label b to a + 1

    let add a b = a + b   // binding the label add to a function


> F# has a really powerful inference engine

---

### var is not let


**_let_** binds an _immutable_ value to a symbol.


**_var_** declares a _variable_ that can be modified later.



> PS: in F# Interactive (or FSI) we can "shadow" a binding with a new value


---

### No return statement

> In _FP_ functions always return a value,
> hence the return statement is unnecessary

    let add a b = 
      a + b      // return the sum of a and b
      // or 
      let c = a + b 
      c

--- 

### Scoping and indentation

> F# is a *whitespace-significant* language. Scope is defined by indent the code 

    // C# version
    public int add (int a, int b) 
    {
      return a + b;
    }

    // F# version
    let add a b =
      a + b

---
    
### Scoping and indentation
</br>

    // nested context
    let add a b =
      let inner x =
        x + 1
      b + inner a

    val add : a:int -> b:int -> int

    > add 1 2
    val it : int = 4

---

### Access modifier
</br>

*[Reminder]* F# is functional first 

* By default the access level is **Public**
* **static** is the default vay for F#
* Use nested scope to protect values and functions

---

## Now you try [1]
</br>

* Get acquainted with the [F# type system and basic concepts](https://en.wikibooks.org/wiki/F_Sharp_Programming/Basic_Concepts)
* [F# Cheat Sheet](http://dungpa.github.io/fsharp-cheatsheet/)
* Create your own functions with nested scope


***

## F# Intro (Part 2)

---

### Type Inference

* F# is a **strongly typed** language
* You don't need to declare the type, the **compiler** will _infere_ the type for you (most of the time)
* Making your types explicit could help the readability of the code (and the compiler in some complex scenarios)

---


### Type Inference


    > let a = 1
    val a : int = 1
    > let s = "Stefano"
    val s : string = "Stefano"
    // double float (64 bit)
    > let b = 10.3
    val b : float = 10.3
    > let b1 = 1.3f
    val b : float32 = 1.3
    > let aa = (1, 2)
    val aa : int * int = (1, 2)
---

### Type Inference
</br>

    // Array (mark the syntax)
    > [|1; 2; 3|];;
    val it : int [] = [|1; 2; 3|]
    // List (we will come back on list)
    > [1;2;3]    
    val it : int list = [1; 2; 3]
    // Empty generic list
    > []
    val it : 'a list
    // Tuples and List (BE AWARE)
    > [1;2;3;4;5;6] 
    val it : int list = [1; 2; 3; 4; 5; 6]
    > [(1,2,3);(4,5,6)]
    val it : (int * int * int) list = [(1, 2, 3); (4, 5, 6)]



---

### Type Inference (functions)
</br>

    > let fun1 a b =  
        a + b
    val fun1 : a:int -> b:int -> int

    > let hello name = 
        sprintfn "Hello %s" name
    val hello : name:string -> string

---

### Type Inference (functions)
</br>

    > let AddToList first second = 
        first :: second :: []
    val AddToList : first:'a -> second:'a -> 'a list

    > AddToList 1 2
    val it : int list = [1; 2]
    > AddToList "Stefano" "Paluello"
    val it : string list = ["Stefano"; "Paluello"]

---

### Type Inference with annotate

    > let sumf (a:float)(b: float) = 
        a + b
    val sumf : a:float -> b:float -> float
    > sumf 1 2

      sumf 1 2
      -----^

    error FS0001: This expression was expected to have type
        'float'    
    but here has type
        'int'    

    > sumf 1.0 2.0
    val it : float = 3.0


---

### Type Inference and recursive functions


    > let rec fact x =       
        if x <= 1 then 1     
        else x * fact (x - 1);;
    val fact : x:int -> int

    > let rec even x = 
        if x = 0 then true
        else odd (x - 1) 
      
      and odd x = 
        if x = 0 then true
        else even (x - 1)
    val even : x:int -> bool
    val odd : x:int -> bool


* **_rec_** is the reserved keyword to identify a recursive function

---

### Complex types (Tuples)


    > (1,2)
    val it : int * int = (1, 2)
    > ("one", "two", "three") // not only two...
    val it : string * string * string = ("one", "two", "three")
    > ("one", 1, 2.0)  // different types
    val it : string * int * float = ("one", 1, 2.0)
    > (a + 1, b + 2)   // tuples of expressions
    val it : int * int = (2, 4)A

    > let (a, b) = t
    val b : int = 2
    val a : int = 1
    > let a, b = t
    val b : int = 2
    val a : int = 1


* A **tuple** is *a grouping of unnamed but ordered values* 



--- 

### Complex types (Records)

    > type Point = { X: float; Y: float; Z: float; }
    type Point =
      {X: float;
      Y: float;
      Z: float;}
    
    [<Struct>]
    type StructPoint = 
        { X: float
          Y: float
          Z: float }

    // inference on records
    > let p = { X = 1.0; Y = 2.0; Z = 3.0 };;
    val p : Point = {X = 1.0;
                    Y = 2.0;
                    Z = 3.0;}


---

### Now you try [2]

* Use [Exercism](http://www.exercism.io)
* Solve **at least** the first 3 exercises (Hello World, Leap, Bob)
* Share your code ;-) 


***

# Part 2

---

### Mutable and Immutable
</br>

> F# is a functional first language, meaning the default behaviour is immutability.

</br>

 PS: You can still use mutable variable, you just need to make it explicit (something like *"Are you really sure you want that?"*)

---

### Mutable and Immutable
</br>

> Making **mutable explicit**
</br>

    > let mutable a = 0    // explicit declaration 
    val mutable a : int = 0

    > i <- 2

</br>
PS:  you could need mutable objects mainly wheh you deal with .Net libraries from another language (eg. C#) 


---

### Mutable and Immutable

> ** PERFORMANCE **

- Immutable means making a copy every time you pass your data around. It sounds expensive, and for a side it is (much less than we think).
- On top of that, nowadays, most of the languages have optimized data structures for immutable data. 

---

### Benefits of Immutability

- It fits very well with the concurrent/multithread/scalable programming paradigm cause you avoid lock and mutable shared data
- Your data are protected, hence you don't need (or need less) encapsulation or other hiding mechanism

---


### Expressions and statements
</br>

- What is a statement?

> A statement is a set of actions that a program takes.

- Common actions include declaring variables, assigning values, calling methods, looping through collections, and branching to one or another block of code, depending on a given condition.

<div style="text-align: right"><a href="https://msdn.microsoft.com/en-us/library/ms173143.aspx">Source: MSDN</a></div>


---

### Expressions and statements
</br>

    // statement in C#
    public void IfStatement(bool aBool)
    {
        int result;
        if (aBool)
        {
            result = 13;
        }
        Console.WriteLine("result={0}", result);
      }

---

### Expressions and statements
</br>

- What is an expression?

> A sequence of operands (1+) and operators (0+) that can be evaluated to a single value, object, method, or namespace.

---

### Expressions and statements
</br>

    // expression directly from the 
    // the previous statement (and in F#)
    let IfStatement aBool =
        let mutable result = 0
        if (aBool) then result <- 13
        printfn "result=%i" result


- We are using mutable and is not really nice code

---

### Expressions and statements
</br>

    // more "functional" expression
    let IfThenElseExpression aBool =
        let result = if aBool then 13 else 0
        printfn "result=%i" result


- Using immutable value and with the if using also the else branch (otherwise we will get an error)

---

### Expressions and statements
</br>

#### Recap


 **Type** | **Return** | **Side-effects** 
--- | --- | ---
Statements | No | Yes
Expressions | Yes | No

---

### The unit type

- F# incourages the use of expressions
- Expressions always return a value
- _Unit_ is a type that indicates the absence of a specific value


    > ()
    val it : unit = ()
    > let printIt x = printfn "%A" x
    val printIt : x:'a -> unit


---

### Ignore function

- Sometimes we need to discard the value of an expression
- we can assing the value to a symbol we won't use 
- ... or we can use ignore


    // signature
    ignore : 'T -> unit

    > let add a b = a + b
    val add : a:int -> b:int -> int
    > add 1 2 |> ignore
    val it : unit = ()

---

## Now you try [3]
</br>

* Write a loop the OOP way (while/for and a counter variable)
* Write the same code in a more **_idiomatic_** way: no loop and _ONLY_ immutable data


***

# Dive into F#

---

### Algebraic Data Types

A new composite type, or a type formed combining other types in 2 way:

- A combinations of types, to crete a new one: a _"product"_ type
- A dijoint union representing a choice between a set of types: a "sum" type


--- 

### Algebraic Data Types

> Product type

    > type MyType = { description : string; aCheck : bool }
    > let mt = { description="just a string"; aCheck = true }

    val mt : MyType = {description = "just a string";
                      aCheck = true;}

--- 

### Algebraic Data Types

> Sum type (in category theory and more general a coproduct, or categorical sum type) 

    > type ThisOrThat =
          | This of int
          | That of bool

    > let a = This 1;;    
    val a : ThisOrThat = This 1

    > let b = That false;;
    val b : ThisOrThat = That false


--- 

### Algebraic Data Types

> Sum type: Introducing Option

    type Option<'T> =
      | Some of 'T
      | None
    
--- 

### Algebraic Data Types

> Sum type: Use of Option


    > let a = Some 1
    val a : int option = Some 1

    > let b = Some "Hello Option"
    val b : string option = Some "Hello Option"

    > let c = None
    val c : 'a option

    > let d : Option<int> = None
    val d : Option<int> = None


---

### Pattern Matching

> Patterns are rules for transforming input data. 


- In F# patterns are used when we use match expression, when we are processing arguments for functions in let bindings, lambda expressions, and in the exception handlers associated with the try...


    match expression with
    | pattern [ when condition ] -> result-expression
    ...


---

### Pattern Matching

> match is like a switch on steroids


    let retValue = ExternalProcess par1
    match retValue with 
    | Some x -> // do something with x
    | None -> // no results, do something for that


    let myList = GetAlist par1
    match myList with 
    | [] -> // code for empty list
    | x :: xs -> // cons (or singly-linked) list 
                 // x is head ('T), xs is the tail (List<'T>)

---

### Pattern Matching

    // another example
    type State = New | Draft | Published | Inactive | Discontinued
    let handleState state =
      match state with
      | Inactive -> () // code for Inactive
      | Draft -> () // code for Draft
      | New -> () // code for New
      | Discontinued -> () // code for Discontinued

--- 

### Exhaustive Pattern Matching

- The use of pattern matching with F# helps to catch a lot of runtime error at compile time


    let myOption = Some 1
    match myOption with 
    | Some x -> printfn "Got a %A" x

    warning FS0025: Incomplete pattern matches on this expression.
    For example, the value 'None' may indicate a case not covered by the pattern(s).


---

### [Advanced] Active Patterns

> Special type of Pattern Matching 

    let (|Int|_|) str =
    match System.Int32.TryParse(str) with
    | (true,int) -> Some(int)
    | _ -> None

    let (|Bool|_|) str =
    match System.Boolean.TryParse(str) with
    | (true,bool) -> Some(bool)
    | _ -> None

---

### [Advanced] Active Patterns


    let testParse str =
      match str with
      | Int i -> printfn "The value is an int '%i'" i
      | Bool b -> printfn "The value is a bool '%b'" b
      | _ -> printfn "The value '%s' is something else" str
    
    testParse "12"
    testParse "true"
    testParse "abc"


---

## Now you try [4]
</br>

- Write your own version of the FizzBuzz Challenge using Active patterns and idiomatic F#

***

# Part 3
</br>

###  Functions: currying and partial application
</br>
</br>

---

###  Currying 
#### (after Haskell Curry)
</br>


    > let sum a b = a + b
    val sum : a:int -> b:int -> int

    // or you can actually write it like this:
    > let sum2 = fun a -> fun b -> a + b
    val sum2 : a:int -> b:int -> int


---

###  Currying 
</br>

    
    > let aa = sum 1
    val aa : (int -> int)
    > let bb = aa 2
    val bb : int = 3

    > let aaa = sum2 1
    val aaa : (int -> int)
    > let bbb = aaa 2
    val bbb : int = 3


---

###  Play with the syntax...
</br>


    let x = 6
    let y = 7
    let z = x + y
    val z : int = 13 

    // or we can write this way:
    6 |> (fun x ->
      7 |> (fun y ->
        x + y |> (fun z ->
          z)))
    val it : int = 13 // anonymous result..


---

### Partial application
</br>


    > let sum a b c = a + b + c
    val sum : a:int -> b:int -> c:int -> int

    > let add1Then2 = sum 1 2
    val add1Then2 : (int -> int)

    > add1Then2 3
    val it : int = 6


---

## Now you try [5]
</br>

- Write a function that need 3 parameters. Then, write the corresponding curried functions and use them accordingly


    function1 : 'a -> 'b -> 'c -> 'd
    fun1a : ('b -> 'c -> 'd)
    fun1b : ('c -> 'd)
    fun1c : ''d


***

### Pipe Operator


- Syntactic sugar for chained method calls


  > let ( |> ) x f = f x


    > let sum a b = a + b
    > 1 |> sum 2
    // the same as 
    > sum 2 1
    val it : int = 3
    > let add1 = sum 1
    val add1 : (int -> int)

    > 2 |> add1 
    // the same as 
    > add1 2


---

### Functions and pipe operator


    > let l1 = 
          [1;2;3]                      
          |> List.map(add1)            
          |> List.filter(fun x -> x % 2 = 0)
    val l1 : int list = [2; 4]

    > l1 |> List.iter(fun x -> printfn "x = %d" x)
    x = 2
    x = 4
    val it : unit = ()



---

### Compose operator ( >> )


    let (>>) f g x = g(f x)

    // where
    f : 'a -> 'b
    // and
    g : 'b -> 'c

    // so... 
    let h = f >> g
    h : 'a -> 'c 


---

### Compose functions


    > let sum a b = a + b
    val sum : a:int -> b:int -> int
    > let add1 = sum 1
    val add1 : (int -> int)

    > let printRes a = sprintf "Res = %d" a
    val printRes : a:int -> string

    > let sumAndPrint = add1 >> printRes
    val sumAndPrint : (int -> string)

    > sumAndPrint 10
    val it : string = "Res = 11"


- Why compose with _sum_ won't work?


---

### Write functions with multiple params
</br>

- Function syntax for partial application and composition


    The earlier params should be the ones (more likely) to be "fixed"
    The last should be collections, records, data structures (more dynamic)
    Keep the right order for well know/standard methods (eg.: subtract

    // eg. map: function, input list, return list
    List.map : ('a -> 'b) -> list 'a -> list 'b

---

### Function associativity 
</br>

> We have a chain of functions/params, how are they applied?


    let f1 x y z = x (y z)  // right associative

- or 


    let f2 x y z = (x y) z  // left associative


- Which version: f1 or f2 ?


---

### Function associativity 

 And the winner is...
</br>

> **_f2_**  -> F# is _left_ associative
</br>


    let f2 x y z = x y z
    // is the same of
    let f2 x y z = (x y) z


---

### Point free style

    let sum a b = a + b // classic or explicit
    (+) a b           // point free 

    // explicit
    let add3div2 x = (x + 3) / 2
    val add3div2 : x:int -> int

    // or point free...
    let add3div2 = (+) 3 >> (/) 2 
    val add3div2 : (int -> int)

---

### F# combinators


    let (|>) f x = x f
    let (<|) f x = f x
    let (>>) f g x = g (f x)
    let (<<) f g x = f (g x)

---

### F# combinators

    x |> f // the same of f(x)
    f <| x // the same of f(x)

    f >> g // the same of g(f(..))
    g << f // the same of g(f(..))


---

### F# combinators

- f and g are 2 different functions...


    // if I write
    f |> g
    // or I write
    f >> g


- is the result the same? Yes/No, why?


' No, look at the signature. The latter will return a function. The first, if added a param could return a value (scalr)

 
---

### HOF functions
</br>

    // typical example the map function
    List.map : ('a -> 'b) -> List 'a -> List 'b

    let add2 = (+) 2
    val add2 : (int -> int)

    [1;2;3] |> List.map(add2)
    val it : int list = [3; 4; 5]


---


## Now you try [5]
</br>

- Write a calculation with different functions and combine them together with the compose operator:


    - Receive a number (integer)
    - add 2 if odd or 3 if even
    - multiply by 3 
    - square it
    - Print the result to the console with the prefix odd or even regarding the result 


- Pay attention to the different types


***

### Recursive functions

- FP encourage to use recursive functions instead of loops


    // fibonacci (what else?) recursive function
    let rec fibonacci i =   // return the ith fibonacci element
      match i with 
      | 1 -> 1
      | 2 -> 1
      | n -> fibonacci (n-2) + fibonacci (n -1)

---


### Tail recursion


Tail Recursion is a specialized type of recursion where there is a guarantee that nothing is left to execute in the function after a recursive call


    let rec factorial x = 
        match x with 
        | 0 -> x
        | 1 -> x
        | _ -> x * factorial (x-1) 


> Tail recursive or not?


---

### Tail recursion
</br>

- Tail recursive way 



    //We use an internal recursive function
    let factorial x =
      let rec inner y acc =  // using an accumulator acc
        match y with 
        | 0 -> acc
        | 1 -> acc
        | _ -> inner (y -1) (acc * y) 

      inner x 1


---

### Tail recursion
</br>

> Why use tail recursive:


    The function executes slightly faster, since no stack pushes and pops are required
    The function can recurse indefinitely since no stack resources are used
    No StackOverflow exceptions


---

### HOF functions
</br>
</br>

- A higher-order function is a function that takes another function as a parameter, or a function that returns another function as a value, or a function that does both.

---

### HOF functions
</br>

    // passing a function as a parameter
    let f (g: 'a -> 'b) a  = g a
    let f (g: 'a -> 'b) a  = a |> g  // or with the pipe operator
    val f : g:('a -> 'b) -> a:'a -> 'b

    // returning a function 
    let sum1 = (+) 1
    val sum1 : (int -> int)

    // getting and returning a function
    let f (g: 'a -> 'b) a  = g
    val f : g:('a -> 'b) -> a:'a0 -> ('a -> 'b)

---

### Benefits of Functional

- The code becomes more testable. You can call your methods hundreds of time and the result will be always the same
- 
